import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver

Mobile.callTestCase(findTestCase('Test Cases/Register/Block - To Register Page'), [:])

Mobile.scrollToText('Sign Up with Google')

Mobile.tap(findTestObject('Register/Google/btn_sign_up_google'), 3)

Mobile.waitForElementPresent(findTestObject('Register/Google/btn_add_another_account'), 3)

Mobile.tap(findTestObject('Register/Google/btn_add_another_account'), 7)

Mobile.waitForElementPresent(findTestObject('Register/Google/input_username'), 3)

Mobile.setText(findTestObject('Register/Google/input_username'), username, 3)

Mobile.waitForElementPresent(findTestObject('Register/Google/btn_next'), 3)

Mobile.tap(findTestObject('Register/Google/btn_next'), 3)

Mobile.waitForElementPresent(findTestObject('Register/Google/input_password'), 3)

Mobile.setText(findTestObject('Register/Google/input_password'), password, 3)

Mobile.waitForElementPresent(findTestObject('Register/Google/btn_selanjutnya'), 3)

Mobile.tap(findTestObject('Register/Google/btn_selanjutnya'), 3)

Mobile.waitForElementPresent(findTestObject('Register/Google/btn_i_agree'), 3)

Mobile.tap(findTestObject('Register/Google/btn_i_agree'), 3)

Mobile.verifyElementNotExist(findTestObject('Register/btn_login'), 3)

AppiumDriver<?> driver = MobileDriverFactory.getDriver()

driver.terminateApp('com.codingid.codingidhive.betastaging')