<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Change Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>052b225a-c993-4b90-b6de-40cb6d7b8d4d</testSuiteGuid>
   <testCaseLink>
      <guid>6e5a7fec-2939-41ef-aae6-e5a72dfe751f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-1-P</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ca1f7088-acb8-4599-802f-2f1b50766fa4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>83ecd888-e870-4927-a7ec-c6852076deee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-2-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>223be3c9-57a7-4c0e-9e5e-609fad836888</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9ddebad0-71df-43e4-aa5d-1756f1ab672b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-3-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>49bfb625-773a-4438-ac85-99b7563ee517</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>130f68fa-c8c6-440a-ae3f-b32d6e643dd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-4-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fa9c506f-ba27-4cb1-91f1-e0cd0769a65c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5e111ee2-284b-4b48-8923-47a29e3e088d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-5-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a19a64be-1c9a-4706-80c1-13c31f268e49</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>21c9fe5c-509a-4cf1-877c-12d2e5f69939</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-7-P</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6d286886-6118-4d9b-bbfd-1ff439a46ab4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bf1ce719-fd95-45c4-b52c-c274e22b0a67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-8-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e17d58b3-eeec-4b42-a6c6-7a6842dfcb1e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6fffb378-40db-49c7-864d-1a6ce69bf137</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-9-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>85719c14-cca2-4654-b836-2be42d2044e0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>19ac1b65-425e-4473-878d-2e0cd156ce7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-10-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>88e169b9-e907-4724-9d37-2f0c38abe033</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>37fa1009-8346-4881-a9c1-628d7d618f94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-11-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4a7b1384-8c93-4e85-81f7-e40bf1629514</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>26a40bae-bfac-47d6-afca-90d525509ec3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Profile/ADR-CP-14-P</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a059ca16-9075-4c38-a0ba-ca9d5fb5319b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>20fbeb12-89ed-4a0f-aa56-89494470fafc</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
