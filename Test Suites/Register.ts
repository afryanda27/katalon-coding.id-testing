<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>cb14e409-fbae-41fd-bd9e-89e2abba1920</testSuiteGuid>
   <testCaseLink>
      <guid>20dcf82a-19b3-4e1a-93e2-b51731435d63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-9-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8b4168cf-b8b9-4032-8dba-aab9e8e9b616</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24ab64be-1e58-4c61-9ee1-e4de60a90b48</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a72aff06-2291-4e01-ae55-13b9db1e49f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e43e26eb-ca7f-4d3d-afc0-9cadc613b7ac</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a0a6c118-4ef0-4b09-827f-4fc64f61b481</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f67f891b-4c60-4bfa-8774-c4d87224500d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-11-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>43010e6f-52ae-4c52-966e-5e1b9abaf858</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e03095d9-9ac3-4d0a-a9bf-118253d31fb1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e4ef3412-20aa-49a0-903a-12931b504bc8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a0dee283-6954-4fbb-9e20-f01510e5d813</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4e476c38-29d3-49a7-b1e4-e5a421a1c70e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c150edb6-1b1b-4b1a-a3fe-df8cb1f9b045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-12-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>18c5a9b2-fe8e-4d32-a28d-b4a43159c469</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>27941475-d5e5-4beb-915e-2c22e880c8d7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>18d0c194-6db3-4f4c-8719-795133fa359a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>159d257d-a28f-4d1b-9f61-a2b4a2513181</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>14bbf64c-a79f-44dc-8615-5abe4b3f0634</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7c9a4a3b-3fcb-4b36-a16e-eadd028ec558</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-18-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a8b9fed3-fce9-44b9-97cb-7eb050a08f64</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c51f03c1-3440-40db-af77-b0ed03f90d24</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>83e0f34d-49e4-4d05-9b9d-52b583dc5592</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3af2a521-933f-4b5e-b58f-9fd6995bed01</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a829e3fe-50a3-45e8-a337-0ce1a71067ec</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d50dca60-e9aa-47a2-9af1-a84c9ad51f4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-19-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>96b27837-c580-4e89-8ef8-e5534961c2ce</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d2ea13c9-4e99-42d5-a8c9-1ca4e668fe92</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8b363089-50ea-4cf4-86b3-fdcc92984b14</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e36af24f-fca2-4d53-8436-19abd7f6740b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cf979b79-16c5-4a7f-81e9-9a0de61d64e6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cb1aed35-ab64-4acf-983c-10425411bc60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-21-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>39d0e641-7142-4b46-a890-cdfc5eed1c70</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>601d4cde-eff7-44f9-a059-3b4cfcba9688</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>94a9869e-4e91-40fa-8624-71956450db32</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8c9fb7e8-b847-4589-a994-761ee897ad9d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24ed19e7-b89c-42a1-9923-7614f569dd20</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>60a2b819-fec1-4bec-89e8-f2e000f1f074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-15-P</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6e119f2f-7270-439f-80d3-b5c0e550f61b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b5c1ed32-154c-4af7-941b-81cec3784035</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bf9bd4f0-f4ad-439c-91a6-8a85ca2347e9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d32c6196-b91c-44a2-8715-11f692616d00</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7b2ee4a4-eb3d-4264-8a98-1271831f62da</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2715fe81-77bf-474b-a2f4-d7dd54a0db1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-1-P</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1211e1e8-ca19-4543-ac08-f301fcefda4c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bf998c4e-f104-496a-bafa-d3d7e748b2b9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5ea02059-1326-415f-9e15-86070e146e0d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6de0de6b-7792-41a9-9a89-6cccfc0895b6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c08df7a1-c52a-4eb3-ad94-1a8363078411</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>35f5acd1-3fa4-4b3c-add9-d72ee45509bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-17-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>aafa0f5e-7a8e-4e67-b22a-e9031d2e172b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a5033e88-258c-43bb-b794-54011764eb73</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1116346c-af40-4247-826f-fd63d8b20b17</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2f73de85-bd01-47ac-83b4-2e023ad9bc39</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1a82e247-deb2-46fe-9a31-dc62e940c715</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>60d47c6b-b3ab-4ef5-a66f-47ecc69f80e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-13-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2dab861b-c974-4953-9f4e-fffd2ea7d1a8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24235002-fb45-42c2-94b9-755ea6cdbee9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3b67b7e6-4f6d-4847-ae4b-0c568f64b734</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>de31c001-3a03-4f29-9481-d0f910e5affe</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>685a98b0-439b-4ec0-a04f-2fed9ae15a3a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>01013146-aa15-4c12-b9f7-0c68d0bb2bc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-22-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7132b1ac-5806-4278-a069-0a990db736dc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>959314a1-b35f-4a1c-ac08-1a98e8717333</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>519ecba9-c7e4-4a3b-ab03-bdf56ee6baf3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>646b7ca1-bd80-4a1d-8a59-55e31e3bcf23</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c495a65f-5484-4e30-826c-3ea4c063d100</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ce7fd117-f6e3-4ceb-9b4d-a5aeba5042ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-20-N</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8fd19f6e-0fe4-4399-a7af-fceb5602aca3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>eb0e24b0-a2ad-4803-943d-fb3bba67c89d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>351c5028-b17a-421b-b618-71eeec6a2927</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>da6df1f4-566f-4a8f-84b4-4e1df7739210</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed2886af-7aee-4ea7-bc0e-c714649bc4f8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ac193182-fca3-45a5-ba25-733176946cc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/ADR-R-14-P</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>26c2edd7-f89f-4616-ad73-99da8fea201b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9f68f1f3-95c3-4ca5-b32d-0a1eba30e648</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
